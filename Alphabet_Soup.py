# Solution to Enlighten IT's "Alphabet Soup" challenge
# Challenge description at: https://gitlab.com/enlighten-challenge/alphabet-soup
# Solution provided by Nikola Slavov
#
# Runs on GL linux 4.19.13-200.fcb.x86_64
# Python version 3.6.7
# Compile this program with "python3 Alphabet_Soup.py -s testFile.txt"
# Compile it on Windows 10 with "python Alphabet_Soup.py -s testFile.txt"
import argparse

# some code to handle command line arguments
parser = argparse.ArgumentParser("Solution to the Alphabet Soup coding challenge")
parser.add_argument("-s", help="specify the input file, with its path if not in the same directory")
args = parser.parse_args()
if not args.s:
	print("Error: Missing argument for input file! Please use \"-s FileName.txt\"")
	exit()

# main is the first function to run and it calls the other functions
def main():
	f = open(args.s, "r")
	state = matr_init()
	
	soup = state[0]			# the alphabet soup matrix is in the first element
	words = state[1]		# and the list of words to search for is in the second
	
	location = []
	for word in words:		# search each word and print the results
		location = find_w(word,soup)
		print(f'{word} {location[0]}:{location[1]} {location[2]}:{location[3]}')
	# Note: it ptints out -1 for coordinates if the word is not found

# pulls from the file the size operands, the alphabet soup, and the list of words, returns all of that in a list
def matr_init():
	state = []
	f = open(args.s, "r")								# the file name is pulled from the argument passed to the program
	
	line = f.readline()
	m = int(line[0])									# retrieve the alphabet soup dimensions(1st and 3rd character of 1st line)
	n = int(line[2])
	
	state.append([])
	for index in range(m):								# loops as many times as there are rows in the alphabet soup
		line = f.readline()
		state[0].append([])
		for index2 in range(n):							# loops as many times as there are columns in the alphabet soup
			state[0][index].append(line[index2*2])		# because there are white spaces between the letters in the format here, we index *2 into the line
	
	state.append([])
	line = f.readline()
	while line != "":
		if (line[-1] == '\n'):
			state[1].append(line[0:-1])					# the readline function puts a '\n' character at the end of the string,
		else:											# except when that is the last line of the file
			state[1].append(line)
		line = f.readline()
	
	f.close()
	return state

# find a given word in a given soup and return the start and end coordinates
def find_w(word,soup):
	if len(word) <= 0 or len(soup) <= 0:
		return [-1,-1,-1,-1]												# if given bad data, return -1 coordinates
	
	directions = [[-1,-1],[-1,0],[0,-1],[-1,1],[1,-1],[1,0],[0,1],[1,1]]	# from a good starting position, the other letters in the word can be in one of these 8 directions
	
	for row in range(len(soup)):											# check each letter in the soup against the first letter of the word
		for column in range(len(soup[row])):
			if soup[row][column] == word[0]:
				if len(word) == 1:											# if it matches and len is 1, return the one letter
					return [row,column,row,column]
				for direction in directions:								# otherwise, check in the 8 directions, starting from this letter
					for index in range(1,len(word)):						# for each direction, we try to match len(word) number of letters
						row_e = row + index*direction[0]					# row_e and column_e are the current character in the soup that we are checking,
						column_e = column + index*direction[1]				# all other characters so far (in this direction) must have matched
						
						if row_e < 0 or column_e < 0 or row_e >= len(soup) or column_e >= len(soup[row]):	# if we went out of bounds or the current 
							break																			# character isn't a match, then
						if soup[row_e][column_e] != word[index]:											# we can iterate to the next
							break																			# of the 8 directions
						
						if index == len(word)-1:															# if the character is a match and is the last we need to check, we've found the word
							return [row,column,row_e,column_e]
	
	return [-1,-1,-1,-1]													# if nothing was found in the entire soup, return -1 coordinates

main()	# call main